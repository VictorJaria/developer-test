# README #

### What is this repository for? ###

* Developer Test : Manufacturing Beers
* 1.0

### To deploy in a docker###

* We need to have de Docker installed to deploy de app
*
* First, to compile the app we need to Run As like a Maven Build, in my case I used the Eclipse IDE and run the command "clean compile install" or run in our cmb "mvn clean compile install"
*
* Once we have the jar, we have to go to our cmd and in our folder project path (in my case F:\bitbucket\ManufacturingBeers\developer-test) run the next command to generate our docker image:
*
* docker build -t developer-test .
*
* Once we have the docker image we have to run the next command to run the app: 
*
* docker run -p8080:8080 developer-test
*
* As we see, is runned in the 8080 port. Now we have the application running
*
### Things to keep in mind ###
* 1) In the WebSecurityConfig class we can see that the only requests that are necessary to be logged are de update requests, we can change it with the antMatchers
*
* 2) We can login using the browser in http://localhost:8080/login which redirects to a login.html page, or using the postman with the post request
*    http://localhost:8080/login?username=user&password=password  . As we can see, the username is "user" and the password is "password"
*
* 3) A h2-console is integrated in the Spring Boot App, to make easier the Persistence Layer. We can access to the h2-console with the url 
*    http://localhost:8080/h2-console/  , and a form will appear. The user is "sa" and the password is "password", both are by deffault. And
*    VERY IMPORTANT, we need to change the "JDBC URL" before connect, we need to put "jdbc:h2:mem:testdb" . Is in the application.properties, just in case.
* 
* 4) An example of pagination and sorting url: http://localhost:8080/beers/listPageable?page=0&size=3&sort=description 
*  What means first page, 3 elements and sorted by description atribute

### The developing test adventure ###

* The first day I tried to refresh my memory with the Docker because I used one time but not much, and preparing the bases of the application. 
* Then I start the first mandatory tasks, and start thinking and doing in the sort, the pagination, and the login as the next steps I wanted to achive
* And when all this was coming out, I started thinking about the image task, and finally get it. I did something related with that, but different
* I would have liked to make an html with the images of the beers, to be able to see them, but time is limited. But that's simple I thin
