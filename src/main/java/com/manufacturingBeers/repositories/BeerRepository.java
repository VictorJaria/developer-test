package com.manufacturingBeers.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.manufacturingBeers.model.Beer;

@Repository
public interface BeerRepository extends JpaRepository<Beer, Integer> {

	@Query("SELECT beer FROM Beer beer")
	public List<Beer> getAllBeers();
	
	@Query("SELECT beer FROM Beer beer WHERE beer.id = :idBeer ")
	public Beer findBeerById(@Param("idBeer") Integer idBeer);

}
