package com.manufacturingBeers.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.manufacturingBeers.model.Manufacturer;

@Repository
public interface ManufacturerRepository extends JpaRepository<Manufacturer, Integer> {

	@Query("SELECT manufacturers FROM Manufacturer manufacturers")
	public List<Manufacturer> getAllManufacturers();
	
	@Query("SELECT manufacturer FROM Manufacturer manufacturer WHERE manufacturer.id = :idManufacturer ")
	public Manufacturer findManufacturerById(@Param("idManufacturer") Integer idManufacturer);
}
