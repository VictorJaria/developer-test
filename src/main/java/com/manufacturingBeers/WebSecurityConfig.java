package com.manufacturingBeers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http
				.authorizeRequests()
				.antMatchers("/h2-console").permitAll()
				.antMatchers("/h2-console/**").permitAll()
				.antMatchers(HttpMethod.GET,"/**").permitAll()
				.antMatchers(HttpMethod.POST,"/**").permitAll()
				.antMatchers(HttpMethod.DELETE,"/**").permitAll()
				.antMatchers("/*.jsp").permitAll()
				.antMatchers("/*.do").permitAll()
				.anyRequest().authenticated()
				.and()
          		.formLogin()
                 .loginPage("/login")
                 .successHandler(successHandler())
                 .permitAll()
                 .and()
             .logout()
                 .permitAll();
		
		http.csrf().disable();
		http.headers().frameOptions().disable();
	}
	
	@Bean
	public AuthenticationSuccessHandler successHandler() {
	    return new MyCustomLoginSuccessHandler("/logged");
	}
	
	@Configuration
    protected static class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {
        @Override
        public void init(AuthenticationManagerBuilder auth) throws Exception {
            auth
                    .inMemoryAuthentication()
                    .withUser("user").password("{noop}password").roles("USER");
        }
    }
}
