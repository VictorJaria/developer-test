package com.manufacturingBeers.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.manufacturingBeers.model.Beer;
import com.manufacturingBeers.services.BeerService;

@RestController
@RequestMapping(value = "/beers")
public class BeerController {
	
	@Autowired
	private BeerService service;
	
	@GetMapping(value = "/listPageable")
	public Page<Beer> getManufacturersPageable(Pageable pageable) {
		return service.findAll(pageable);
	}
	
	@GetMapping(value = "")
	public ResponseEntity<List<Beer>> getBeers() {
		List<Beer> beers = service.getBeers();
		return new ResponseEntity<List<Beer>>(beers, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{idBeer}")
	public ResponseEntity<Beer> getBeerById(@PathVariable Integer idBeer) {
		Beer beer = service.getBeer(idBeer);
		if (beer == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Beer>(beer, HttpStatus.OK);
	}
	
	@PostMapping(value = "/insertBeer")
	public ResponseEntity<Beer> newBeer(@RequestBody Beer beer) {
		Beer b = service.insertBeer(beer);
		return ResponseEntity.status(HttpStatus.CREATED).body(b);
	}
	
	@PutMapping(value = "/updateBeer")
	public ResponseEntity<Beer> updateBeer(@RequestBody Beer beer) {
		Beer b = service.updateBeer(beer);
		return new ResponseEntity<Beer>(b, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "deleteBeer/{idBeer}" /*, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE }*/)
	public void deleteBeer(@PathVariable Integer idBeer) {
		service.deleteBeer(idBeer);
	}
	
	@PostMapping("/saveBeerWithImage")
    public String saveBeerWithImage(@RequestParam("name") String name,
    		@RequestParam("graduation") Integer graduation,
    		@RequestParam("type") String type,
    		@RequestParam("description") String description,
    		@RequestParam("file") MultipartFile file)
    {
		System.out.println("Entra saveBeerWithImage");
		service.saveBeerWithImage(file, name, description, graduation, type);
		return "redirect:/beers";
    }
	

	
	
}
