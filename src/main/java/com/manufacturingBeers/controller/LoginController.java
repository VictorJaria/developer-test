package com.manufacturingBeers.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class LoginController {
	
	@GetMapping("/login")
    public String login() {
        return "login";
    }
	
	@GetMapping("/logged")
    public String logged() {
        return "logged";
    }
	
	@GetMapping("/addBeer")
    public String addBeer() {
        return "addBeer";
    }

}
