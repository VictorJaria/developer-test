package com.manufacturingBeers.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manufacturingBeers.model.Manufacturer;
import com.manufacturingBeers.services.ManufacturerService;

@RestController
@RequestMapping(value = "/manufacturers")
public class ManufacturerController {
	
	@Autowired
	private ManufacturerService service;
	
	@GetMapping(value = "/listPageable")
	public Page<Manufacturer> getManufacturersPageable(Pageable pageable) {
		return service.findAll(pageable);
	}
	
	@GetMapping(value = "")
	public ResponseEntity<List<Manufacturer>> getManufacturers() {
		List<Manufacturer> manufacturers = service.getManufacturers();
		return new ResponseEntity<List<Manufacturer>>(manufacturers, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{idManufacturer}")
	public ResponseEntity<Manufacturer> getManufacturerById(@PathVariable Integer idManufacturer) {
		Manufacturer manufacturer = service.getManufacturer(idManufacturer);
		if (manufacturer == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Manufacturer>(manufacturer, HttpStatus.OK);
	}
	
	@PostMapping(value = "/insertManufacturer")
	public ResponseEntity<Manufacturer> newManufacturer(@RequestBody Manufacturer manufacturer) {
		Manufacturer m = service.insertManufacturer(manufacturer);
		return ResponseEntity.status(HttpStatus.CREATED).body(m);
	}
	
	@PutMapping(value = "/updateManufacturer")
	public ResponseEntity<Manufacturer> updateManufacturer(@RequestBody Manufacturer manufacturer) {
		Manufacturer m = service.updateManufacturer(manufacturer);
		return new ResponseEntity<Manufacturer>(m, HttpStatus.OK);		 
	}
	
	@DeleteMapping(value = "deleteManufacturer/{idManufacturer}" /*, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE }*/)
	public void deleteManufacturer(@PathVariable Integer idManufacturer) {
		service.deleteManufacturer(idManufacturer);
	}

}
