package com.manufacturingBeers.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import com.manufacturingBeers.model.Beer;
import com.manufacturingBeers.repositories.BeerRepository;

@Service
public class BeerService {

	@Autowired
	BeerRepository beerRepo;
	
	public List<Beer> getBeers() {
		List<Beer> beers = beerRepo.getAllBeers();
		return beers;
	}

	public Beer getBeer(Integer idBeer) {
		return beerRepo.findBeerById(idBeer);
	}

	public Beer insertBeer(Beer beer) {
		return beerRepo.save(beer);
	}

	public Beer updateBeer(Beer beer) {
		Beer b = null;
		try {
			b = beerRepo.findById( beer.getIdBeer()).orElseThrow(() -> new Exception("Entity not found with Beer id : " + beer.getIdBeer() ) );
		} catch (Exception e) {
			throw new ResponseStatusException(
			          HttpStatus.NOT_FOUND, "Beer Entity Not Found ", e);
		}
		
		b.setDescription(beer.getDescription());
		b.setName(beer.getName());
		b.setManufacturerId(beer.getManufacturerId());
		b.setGraduation(beer.getGraduation());
		return beerRepo.save(b);
	}

	public void deleteBeer(Integer idBeer) {
		beerRepo.deleteById(idBeer);
	}

	public Page<Beer> findAll(Pageable pageable) {
		return beerRepo.findAll(pageable);
	}
	
	public void saveBeerWithImage(MultipartFile file, String name, String description, Integer graduation, String type) {
		Beer b = new Beer();
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		if(fileName.contains(".."))
		{
			System.out.println("not a a valid file");
		}
		try {
			b.setImage(Base64.getEncoder().encodeToString(file.getBytes()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		b.setGraduation(graduation);
		b.setType(type);
		b.setDescription(description);
        b.setName(name);
        
        beerRepo.save(b);
	}
	
}
