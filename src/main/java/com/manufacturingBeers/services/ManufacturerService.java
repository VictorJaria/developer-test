package com.manufacturingBeers.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.manufacturingBeers.model.Manufacturer;
import com.manufacturingBeers.repositories.ManufacturerRepository;

@Service
public class ManufacturerService {
	
	@Autowired
	ManufacturerRepository manufacturedRepo;
	
	public List<Manufacturer> getManufacturers() {
		return manufacturedRepo.getAllManufacturers();
	}
	
	public Manufacturer getManufacturer(Integer idManufacturer) {
		return manufacturedRepo.findManufacturerById(idManufacturer);
	}

	public Manufacturer insertManufacturer(Manufacturer manufacturer) {
		return manufacturedRepo.save(manufacturer);
	}

	public Manufacturer updateManufacturer(Manufacturer manufacturer) {
		Manufacturer m = null;
		try {
			m = manufacturedRepo.findById( manufacturer.getIdManufacturer() ).orElseThrow(() -> new Exception("Entity not found with Manufacturer id : " + manufacturer.getIdManufacturer() ) );
		} catch (Exception e) {
			throw new ResponseStatusException(
			          HttpStatus.NOT_FOUND, "Manufacturer Entity Not Found ", e);
		}

		m.setName(manufacturer.getName());
		m.setNationality(manufacturer.getNationality());
		return manufacturedRepo.save(m);

	}

	public void deleteManufacturer(Integer idManufacturer) {
		manufacturedRepo.deleteById(idManufacturer);
	}

	public Page<Manufacturer> findAll(Pageable pageable) {
		return manufacturedRepo.findAll(pageable);
	}

}
