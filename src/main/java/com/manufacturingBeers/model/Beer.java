package com.manufacturingBeers.model;

import javax.persistence.*;

@Entity
public class Beer {

	@Id
	@SequenceGenerator(name = "BEER_GENERATOR", sequenceName = "SEQ_BEER", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BEER_GENERATOR")
	private int idBeer;
	private String name;
	
	private Integer graduation;
	
	private String type;
	
	private String description;
	
	@Lob
	@Column(columnDefinition = "MEDIUMBLOB")
	private String image;
	
	@JoinColumn(name="MANUFACTURER_ID")
	private Integer manufacturerId;

	public int getIdBeer() {
		return idBeer;
	}

	public void setIdBeer(int idBeer) {
		this.idBeer = idBeer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getGraduation() {
		return graduation;
	}

	public void setGraduation(Integer graduation) {
		this.graduation = graduation;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getManufacturerId() {
		return manufacturerId;
	}

	public void setManufacturerId(Integer manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	
	
}
