package com.manufacturingBeers;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@ComponentScan("com.manufacturingBeers")
@EnableJpaRepositories("com.manufacturingBeers.repositories")
@RestController
public class ManufacturingApplication {

	  @RequestMapping("/")
	  public String home() {
	    return "Hello Docker World!";
	  }

	  public static void main(String[] args) {
	    SpringApplication.run(ManufacturingApplication.class, args);
	  }
}
